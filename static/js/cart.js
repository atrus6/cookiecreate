function get_cart_dict() {
  //Get existing cart
  const cookieValue = document.cookie
    .split("; ")
    .find((row) => row.startsWith("cart="))
    ?.split("=")[1] ?? "{}";
  return JSON.parse(cookieValue);
}

function save_cart_dict(cart_dict) {
  const cv = "cart=" + JSON.stringify(cart_dict)

  document.cookie = `${cv};path=/`;
}

function items_in_cart() {
  cart_dict = get_cart_dict();
  
  var total = 0;

  for (const [key, value] of Object.entries(cart_dict)) {
    total += value;
  }

  return total;
}

function add_to_cart(stripeid, quantity) {
  var cart_dict = get_cart_dict();

  cart_quantity = cart_dict[stripeid] ?? 0;

  cart_quantity += Number(quantity);
  cart_dict[stripeid] = cart_quantity;

  if(cart_quantity <= 0) {
    delete cart_dict[stripeid];
  }

  save_cart_dict(cart_dict)
}

function set_cart_amount(stripeid, amount) {
  var cart_dict = get_cart_dict()

  if(amount <= 0) {
    delete cart_dict[stripeid]
  } else {
    cart_dict[stripeid] = Number(amount);
  }

  save_cart_dict(cart_dict);
}

function remove_from_cart(stripeid) {
  add_to_cart(stripeid, -(get_cart_amount(stripeid)));
}

function get_cart_amount(stripeid) {
  var cart_dict = get_cart_dict();
  return cart_dict[stripeid] ?? 0;
}

function clear_cart() {
  document.cookie = "cart={};path=/";
}

function update_cart_tag() {
  document.getElementById("cartAmount").textContent = items_in_cart();
}

from flask import Blueprint, render_template

from flask import make_response, request
from urllib.parse import urlparse

from app import app
from models import Product, Category

from datetime import timedelta, date
import random

details = Blueprint("details", __name__)

@details.route('/about')
def about():
    return render_template("details/about.html")

@details.route('/faq')
def faq():
    return render_template("details/faq.html")

@details.route('/recipes')
def recipes():
    return render_template("details/recipes.html")

@details.route('/contact')
def contact():
    return render_template("details/contact.html")

@details.route('/shipping_policy')
def shipping():
    return render_template("details/shipping.html")

@details.route("/return_policy")
def returns():
    return render_template("details/returns.html")

@details.route("/privacy_policy")
def privacy():
    return render_template("details/privacy.html")

@details.route("/sitemap.xml")
def sitemap():
    """
        Route to dynamically generate a sitemap of your website/application.
        lastmod and priority tags omitted on static pages.
        lastmod included on dynamic content such as blog posts.
    """

    host_components = urlparse(request.host_url)
    host_base = host_components.scheme + "://" + host_components.netloc

    # Static routes with static content
    static_urls = list()
    for rule in app.url_map.iter_rules():
        if not str(rule).startswith("/admin") and not str(rule).startswith("/user"):
            if "GET" in rule.methods and len(rule.arguments) == 0:
                url = {
                    "loc": f"{host_base}{str(rule)}",
                    "lastmod": str(date.today() - timedelta(days=random.randint(1, 45)))
                }
                static_urls.append(url)

    # Dynamic routes with dynamic content
    dynamic_urls = list()
    products = Product.query.all()
    for product in products:
        url = {
            "loc": f"{host_base}/product/{product.sku}",
            "lastmod": str(date.today() - timedelta(days=random.randint(1, 45)))
            }
        dynamic_urls.append(url)

    categories = Category.query.all()

    for category in categories:
        url = {
            "loc": f"{host_base}/category/cookie/{category.name.replace(' ', '_')}",
            "lastmod": str(date.today() - timedelta(days=random.randint(1, 45)))
        }
        dynamic_urls.append(url)

    xml_sitemap = render_template("details/sitemap.xml", static_urls=static_urls, dynamic_urls=dynamic_urls, host_base=host_base)
    response = make_response(xml_sitemap)
    response.headers["Content-Type"] = "application/xml"

    return response
import os
import json
from collections import defaultdict
from typing import List

from flask import Blueprint, render_template, redirect, request, abort, send_file

import stripe

from app import app, db
from models import Product, Category, Images, get_or_create, BlogPost

from sqlalchemy.sql.expression import func
shop = Blueprint("shop", __name__)


if app.debug:
    stripe.api_key = os.environ['STRIPE_TEST_KEY']
    base_url = "http://127.0.0.1:5000"
else:
    stripe.api_key = os.environ['STRIPE_LIVE_KEY']
    base_url = "https://cookiecreate.com"

def get_price(product):
    x = stripe.Product.retrieve(product)
    return x['default_price']

def get_cookie_cards(cookies:List[Product]) -> List[str]:
    rv:List[str] = []
    for cutter in cookies:
        rv.append(render_template("cookie_card.html", 
                                  name=cutter.name, 
                                  sku=cutter.sku,
                                  price=cutter.pretty_price(), 
                                  description=cutter.description, 
                                  image=cutter.images[0].url, 
                                  thumb=cutter.images[0].thumb))
        
    return rv
    
@shop.route("/")
def index():
    best_selling = get_cookie_cards(Product.query.order_by(func.random()).limit(9))
    

    return render_template('index.html', NAME="Home", best_sellers=best_selling)

@shop.route("/product/<string:sku>")
def product_page(sku):
    cutter = Product.query.filter_by(sku=sku).first()

    if cutter != None:
        categories:List[str] = [x.name for x in cutter.categories]
        data = {
            "IMAGES":cutter.images,
            "NAME":cutter.name,
            "SKU":cutter.sku,
            "PRICE":cutter.pretty_price(),
            "DESCRIPTION":cutter.description,
            "STRIPEID":cutter.stripe_id,
            "categories": categories,
        }
        return render_template("product.html", **data)
    else:
        abort(404)
    
@shop.route("/checkout")
def checkout_page():
    cart = request.cookies.get("cart")

    if cart == None:
        cart = ""
        
    cart = json.loads(cart)
    cutters:List[str] = []
    total_price:int = 0

    for stripe_id, amount in cart.items():
        cutter:Product = Product.query.filter_by(stripe_id=stripe_id).first()
        total_price += (cutter.price * amount)
        cutters.append(render_template("checkout_card.html", sku=stripe_id, thumb=cutter.images[0].thumb, name=cutter.name, price=cutter.pretty_price(), amount=amount))

    total:str = "$" + '{:,.2f}'.format(total_price/100.0)

    return render_template("checkout_preview.html", NAME="checkout", cutters=cutters, total_price=total)

@shop.route("/checkout_stripe")
def checkout_stripe():
    cart = request.cookies.get("cart")
    cart = json.loads(cart)
    cutters = []
    total_price = 0

    for key, value in cart.items():
        li = {
            "price": get_price(key),
            "quantity": value,
        }
        total_price += (value * stripe.Price.retrieve(get_price(key))['unit_amount'])
        cutters.append(li)

    if total_price > 1000:
        shipping_options = {
            "shipping_rate_data": {
                "type": "fixed_amount",
                "fixed_amount": {"amount": 0, "currency": "usd"},
                "display_name": "Free shipping",
                "delivery_estimate": {
                    "minimum": {"unit": "business_day", "value": 2},
                    "maximum": {"unit": "business_day", "value": 7},
                },
            },
        }
    else:
        shipping_options = {
            "shipping_rate_data": {
                "type": "fixed_amount",
                "fixed_amount": {"amount": 400, "currency": "usd"},
                "display_name": "Ground shipping",
                "delivery_estimate": {
                    "minimum": {"unit": "business_day", "value": 2},
                    "maximum": {"unit": "business_day", "value": 7},
                },
            },
        }
    
    try:
        checkout_session = stripe.checkout.Session.create(
            line_items=cutters,
            mode="payment",
            success_url=base_url+'/thanks',
            cancel_url=base_url+'/checkout',
            shipping_address_collection={"allowed_countries": ["US"]},
            shipping_options=[shipping_options]
        )
    except Exception as e:
        return str(e)
    
    return redirect(checkout_session.url, code=303)

@shop.route("/categories/cookie")
def cookie_categories():
    categories = Category.query.order_by(Category.name).all()

    data:List[str] = []
    for category in categories:
        data.append(category.name)

    res = defaultdict(list)
    for i in data:
        res[i[0]].append(i)
    
    return render_template("categories.html", NAME="Categories", categories=res)

@shop.route("/category/cookie/<string:category>")
def cookie_category_page(category):
    cleaned_name = category.replace("_", " ")
    category = Category.query.filter_by(name=cleaned_name).first()

    cutters = category.products

    rv = get_cookie_cards(cutters)

    return render_template("category_page.html", NAME=cleaned_name, products=rv, which="cookies")

@shop.route("/only_search", methods=["POST"])
def only_search():
    s = request.form.get('search').lower()
    if s == "":
        return ""
    
    print(s)
    
    cutters = Product.query.filter(Product.name.ilike("%{}%".format(s)))

    if cutters.count() == 0:
        return None
    
    rv:List[str] = []
    for cutter in cutters:
        print(cutter)
        rv.append(render_template("cookie_card.html", 
                                  name=cutter.name, 
                                  sku=cutter.sku,
                                  price=cutter.pretty_price(), 
                                  description=cutter.description, 
                                  image=cutter.images[0].url, 
                                  thumb=cutter.images[0].thumb))
    return rv

@shop.route("/search", methods=["POST"])
def search():
    rv = only_search()

    return render_template("search.html", products=rv, search_term=request.form.get('search'))

@shop.route("/thanks")
def thanks():
    return render_template("thanks.html", NAME="Thank you!")

@shop.route("/google_merchant_tsv")
def google_merchant_tsv():
    return send_file("google.txt")
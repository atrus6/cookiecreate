import os
from os.path import isfile,join
import sys
import csv
from datetime import datetime

from typing import List

import stripe

from tqdm import tqdm

from app import db,app
from models import get_or_create, Category, BlogPost, Product, Images

from markdown2 import markdown
import frontmatter
import random
import string

from dotenv import load_dotenv
load_dotenv()

def upload_blog_posts():
    with app.app_context():
        bp_path = "blog_posts"
        
        for f in tqdm(os.listdir(bp_path)):
            if isfile(join(bp_path, f)):
                with open(join(bp_path, f)) as post:
                    post = frontmatter.loads(post.read())
                    slug = post["slug"] + "_" + str(post["id"])
                    get_or_create(db.session, BlogPost, id=post["id"], title=post["title"], date=post["date"], contents=post.content, slug=slug)

def create_google_tsv():
    from google import write_google_sheet
    write_google_sheet()

def upload_to_stripe():
    with open("products.csv") as csvfile:
        if app.debug:
            sid = "Stripe ID Test"
            print('Syncing Test Products')
        else:
            sid = "Stripe ID"
            test_mode = False
            print('Syncing Live Products')
        
        table_rows = csv.DictReader(csvfile)
        update_csv = False
        rows = []

        for row in tqdm(table_rows):
            metadata = {"business": "cookiecreate", "sku": row["SKU"]}
            default_price = {"currency": "USD", "unit_amount": row["Regular price"]}

            # Here we are cleaning up the URLs to point to our server.
            np = []

            for ind in range(1,7):
                column_name = 'Image' + str(ind)

                if row[column_name] != '' and row[column_name] != None:
                    np.append(row[column_name])
            
            
            # Here we are either adding nonexistant products, or updating products on Stripes end.
            if row[sid] in (None, ""):
                res = stripe.Product.create(name=row["Name"], default_price_data=default_price, metadata=metadata, description=row['Description'], images=np, shippable=True)
                row_info = {sid: res['id']}
                row[sid] = res['id']
                update_csv = True
            else:
                pr = stripe.Product.retrieve(row[sid])

                needs_refresh = False

                if pr["description"] != row["Description"]:
                    needs_refresh = True
                    print(pr["description"], row["Description"])
                    print("Description needs updated for ", row["SKU"])
                if pr["metadata"] != metadata:
                    needs_refresh = True
                    print(pr["metadata"], metadata)
                    print("Meta data needs updated for", row["SKU"])
                if pr["images"] != np:
                    print(pr["images"], np)
                    needs_refresh = True
                    print("Images need refreshed for ", row["SKU"])
                if pr["name"] != row["Name"]:
                    needs_refresh = True
                    print(pr["name"], row["Name"])
                    print("Name needs refreshed for ", row["Name"])

                if needs_refresh:
                    stripe.Product.modify(pr["id"], images=np, metadata=metadata, description=row['Description'], name=row['Name'])

            rows.append(row)
            # here we update or create new products
            with app.app_context():
                categories = row["Categories"].lower().split(',')
                dbcat:List[Category] = []

                for category in categories:
                    dbcat.append(get_or_create(db.session, Category, name=category.strip()))

                product = get_or_create(db.session, Product, stripe_id=row[sid])

                for image in np:
                    img = get_or_create(db.session, Images, url=image, thumb=Images.create_thumb_url(image), product_id=product.stripe_id)

                product.name = row["Name"]
                product.sku = row["SKU"]
                product.description = row["Description"]
                product.price = row["Regular price"]
                product.categories = dbcat

                db.session.commit()

        if update_csv:
            with open("products.csv", 'w') as op:
                writer = csv.DictWriter(op, table_rows.fieldnames, lineterminator = '\n')
                writer.writeheader()
                writer.writerows(rows)
                print(rows[444])
                print(len(rows))

def sync_everything():
    print("Uploading Blog Posts")
    upload_blog_posts()
    print("Uploading Products")
    upload_to_stripe()
    print("Writing Google TSV")
    create_google_tsv()
    print('Sync Complete')



        


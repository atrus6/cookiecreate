from flask import Blueprint, render_template, url_for

from models import BlogPost, Product

import re
from itertools import count
import markdown2

from sqlalchemy.sql.expression import func

blog = Blueprint("blog", __name__)

@blog.route('/posts')
def index():
    posts = BlogPost.query.order_by(BlogPost.date.desc()).all()

    return render_template("blog_index.html", posts=posts)

@blog.route('/latest_blog_posts')
def latest_blog_posts():
    latest_blog_posts = BlogPost.query.order_by(BlogPost.date.desc()).limit(5)

    rv:str = "<ul>\n\t"

    for post in latest_blog_posts:
        rv += "<li><a href='" + url_for('blog.get_post', id=post.id) + "'>" + post.title + "</a></li>\n"

    rv += "</ul>"
    return rv

@blog.route('/posts/<id>')
def get_post(id:int):
    products = Product.query.order_by(func.random()).limit(4)
    
    rv:[str] = []
    for product in products:
        rv.append(render_template("cookie_card.html", 
                                  name=product.name, 
                                  sku=product.sku,
                                  price=product.pretty_price(), 
                                  description=product.description, 
                                  image=product.images[0].url, 
                                  thumb=product.images[0].thumb))
    
    post:BlogPost = BlogPost.query.filter_by(id=id).first()
    img:int = 1
    image = count(img)
    content = post.contents.replace('~!~IMG', '~!~IMG<br>')
    content = re.sub('~!~IMG', lambda x: "<img src='https://cookiecreate.nyc3.cdn.digitaloceanspaces.com/blog/" + str(post.id) + "/" + str(next(image)) + ".jpg'>", content)
    content = markdown2.markdown(content)
    content = content.replace('\n\n', '<br>')
    return render_template("blog_post.html", title=post.title, date=post.date, content=content, products=rv)
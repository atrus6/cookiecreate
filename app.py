import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView

from flask_migrate import Migrate

from flask_redmail import RedMail

app = Flask(__name__)

from dotenv import load_dotenv
load_dotenv()

#Configure the sender
app.config["EMAIL_HOST"] = os.environ["EMAIL_HOST"]
app.config["EMAIL_PORT"] = os.environ["EMAIL_PORT"]
app.config["EMAIL_USERNAME"] = os.environ["EMAIL_USER"]
app.config["EMAIL_PASSWORD"] = os.environ["EMAIL_PASSWORD"]
app.config["EMAIL_SENDER"] = os.environ["EMAIL_SENDER"]

# For generating Tokens
app.config["SECRET_KEY"] = os.environ["SECRET_KEY"]

# DB location
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///app_data.db"

# Stripe stuff
app.config["STRIPE_TEST_KEY"] = os.environ["STRIPE_TEST_KEY"]
app.config["STRIPE_LIVE_KEY"] = os.environ["STRIPE_LIVE_KEY"]

app.config["ADMIN_EMAILS"] = os.environ["ADMIN_EMAILS"].split(" ")

db = SQLAlchemy(app)
login_manager = LoginManager()
login_manager.login_view = "auth.login"

login_manager.init_app(app)

email = RedMail(app)


from models import User, product_category_association_table, Order
from shop import shop
from auth import auth
from blog import blog
from details import details
from money import money

app.register_blueprint(shop)
app.register_blueprint(auth)
app.register_blueprint(blog)
app.register_blueprint(details)
app.register_blueprint(money)

from models import UserAdmin, MyModelView
admin = Admin(app)
admin.add_view(UserAdmin(User, db.session))
admin.add_view(MyModelView(Order, db.session))

with app.app_context():
    db.create_all()

migrate = Migrate(app, db)